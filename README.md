# Introduction
This repository contains text files and images used under the name _Proaggelos_,
_Forerunner_ or _Harbinger_. Some of them were used in [https://www.harbinger.gr](https://web.archive.org/web/20161111151325/http://harbinger.gr/)
whereas others were used in https://proaggelos.blogspot.com.