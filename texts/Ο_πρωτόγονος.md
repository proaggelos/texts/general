# Ο πρωτόγονος

![image](https://gitlab.com/proaggelos/texts/general/raw/master/texts/images/o_protogonos.jpg)
	
Βγήκα σήμερα από τη σπηλιά μου. Είναι μία από αυτές τις μέρες που ο θεός Ήλιος δεν έχει θυμώσει μαζί μου και μου χάρισε το φως του. Το προχτεσινό θήραμα τελείωσε μόλις χτες το βράδυ. Αν θέλω λοιπόν να φάω ένα γερό πρωινό, πρέπει να πάω για κυνήγι. Λέω, όμως, σήμερα, να πάω σε μέρη που δεν έχω πάει. Να εξερευνήσω νέες περιοχές και νέα θηράματα. Ας ξεκινήσω λοιπόν...

Να πάω προς τα βουνά ή προς τη θάλασσα; Στο δρόμο προς τα βουνά θα συναντήσω τους ινδιάνους με τα τοτέμ τους. Ωραία γλυπτά, αλλά αυτό το φτερό στο κεφάλι δε με εμπνέει. Σα γαλοπούλες είναι. Από την άλλη, ο δρόμος προς τη θάλασσα μου είναι εντελώς άγνωστος. Μόνο από τον πατέρα μου έχω ακούσει γι' αυτήν. Μου είχε δείξει μια λακκούβα, μια φορά που ο θεός της βροχής μας χάρισε το δώρο του, και μου είχε πει ότι η θάλασσα είναι σαν τη λακκούβα αλλά πολύ μεγαλύτερη.

Θα πάω στη θάλασσα! Μόνο λίγο να ισιώσω το δέρμα του αρνιού στο σώμα μου και είμαι έτοιμος!  Ωραία! Α, να πάρω και το ρόπαλό μου. Τώρα είμαι έτοιμος!

Περπατώ μέσα στα χόρτα και βλέπω τα πουλιά να πετάνε από πάνω μου. Μάλλον φοβισμένα. Αλλά γιατί; Περπατώ λίγο ακόμη και βλέπω στον ορίζοντα κάτι γκρίζο και δίπλα του κάτι πολύ ψηλό και επίσης γκρίζο. Ευτυχώς, μέχρι εκεί, έχει χόρτα, οπότε μπορώ να πλησιάσω, χωρίς να τραβήξω την προσοχή κανενός, σε περίπτωση που οι κάτοικοι εκεί είναι εχθρικοί. Προχωράω σιγά αλλά σταθερά  και πλέον έχω φτάσει δίπλα στο ψηλό γκρίζο πράγμα. Μοιάζουν με τοτέμ. Όμως, μα τον Τουτάτη! Τι παράξενα τοτέμ! Είναι τόσα πολλά. Και είναι παρατεταγμένα δίπλα-δίπλα το ένα στο άλλο! Και ενώνονται με κάτι μαύρα σκοινιά. Αλλά γιατί κανείς δεν προσκυνάει κανένα από αυτά;

Παραδίπλα, κρυμμένος πάντα μέσα στα χόρτα, μπορώ να δω διάφορα χρώματα να πετάνε πάνω από κάτι γκρίζο. Φοβάμαι... Όμως... Κοιτάζοντας καλύτερα, τα χρώματα δεν πετάνε. Βρίσκονται πάνω σε κάτι άλλο γκρίζο που αυτό ακουμπάει στο γκρίζο χώμα κάτω. Όμως, γκρίζο χώμα... Τι περίεργο πράγμα! Μήπως αυτό είναι η θάλασσα; Αλλά ποιον να ρωτήσω; Δεν υπάρχει κανείς. Και να υπήρχε δηλαδή σιγά μην με καταλάβαινε. Γύρω στις 30 φορές είχε σηκωθεί και μετά είχε φύγει ο Ήλιος μέχρι να συνεννοηθούμε με τους Ινδιάνους. Άραγε με τους κατοίκους εδώ, πόσες φορές θα χρειαζόταν!

Μήπως αυτό είναι θάλασσα; Αλλά η λακκούβα δεν ήταν έτσι. Δεν είχε τόσα χρώματα. Έβαζα το χέρι μου μέσα της άφοβα και δημιουργούσα κυκλάκια. Εδώ φοβάμαι να βάλω το χέρι μου. Το βρήκα! Θα χρησιμοποιήσω το ρόπαλό μου!

**Μπαμ**

Τι... Άουτς! Ζαλίστηκα. Τι έγινε; Ωχ ωχ. Ας σηκωθώ. Τι έκανα; Α ναι! Γιατί σταμάτησαν τα χρώματα; Και αυτό το σπασμένο πράσινο κουτί δίπλα στο γκρι τοτέμ τι είναι; Ας πάω να δω!

Είναι και ένας άνθρωπος μέσα. Όσο και να τον κουνάω όμως δε μου δίνει καμιά σημασία. Αν ήμουν άλλος θα τον σκότωνα για την αγένειά του αλλά ίσως να είναι κουρασμένος. Όμως τι να είναι αυτό το πράσινο κουτί όπου ήταν μέσα; Ααα! Τώρα έρχεται ένα μπλε κουτί που έχει πάνω έναν μπλε και έναν κόκκινο ήλιο και ένα παρδαλό κουτί που είναι άσπρο και κόκκινο και έχει δύο κόκκινους ήλιους και κάτι λένε. Κάτι σαν “Ουυυυυιιιιιιιιιιιιιιιιιιιιιουυυυυυυυυ”. Τι θεοί είναι αυτοί; Δεν τους έχω ξανακούσει. Όμως, τα χρώματα πάνω στο γκρίζο χώμα σταμάτησαν. Νομίζω μπορώ να περάσω απέναντι. Τουλάχιστον εκεί έχει πάλι χωράφια.

Ωραία, πέρασα. Ας προχωρήσω λίγο ακόμη. Πφφφ... Κι άλλα τοτέμ. Ή... τι είναι αυτά; Ας πάω πιο κοντά να δω. Κι αυτά είναι σε διάφορα χρώματα όπως τα προηγούμενα κουτιά. Αλλά είναι πιο σκληρά. Πρέπει να είναι κουτιά από πέτρα. Αλλά ίσια πέτρα! Πού τη βρίσκουν άραγε τόση πολύ; Α! Παρακάτω έχει κι άλλο γκρίζο χώμα. Να πάρει! Εδώ περνάνε ακόμη τα κουτιά που περνούσαν και πριν! Αυτή τη φορά όμως δε θα κάνω το ίδιο λάθος. Θα πετάξω το ρόπαλο πάνω στα χρώματα!

**Μπαμ**

Α, ωραία. Σταμάτησαν! Ας μαζέψω το ρόπαλό μου πριν το χάσω. Μα τι είναι αυτό το κρανίο κολλημένο πάνω του; Καλά ποιος πετάει κρανία πάνω στο ρόπαλό μου; Τι αγενείς άνθρωποι! Πριν κοιμόντουσαν! Τώρα μου πετάνε κρανία. Α! Εκεί έχει καφέ χώμα! Έχει και νερό! Αυτή πρέπει να είναι η θάλασσα! Έφτασα! Ουυυυκγκουχ γκουχ. Περπάτησα πολύ όμως για να φτάσω και δίψασα. Πού θα βρω τώρα νερό; Ά! Στη θάλασσα!

Μπλφτουυυυυυ! Τι νερό είναι αυτό; Χφτ! Γκαντεμιά. Δεν έχει τίποτα βρώσιμο εδώ. Πάω σπίτι! Αυτό το νερό φαίνεται πίνουν οι άνθρωποι γι' αυτό είναι τόσο αγενείς εδώ. Πάω σπίτι. Και όσο σκέφτομαι ότι θεωρούσα τους ινδιάνους τρελούς με τα φτερά στο κεφάλι... Καλύτερα με το Γυριστό Αρνί. Καλύτερα....

## Για ό,τι δεν καταλάβατε:
1. Τα “Τοτέμ που ενώνονται με κάτι μαύρα σκοινιά”, όπως τα αναφέρει ο πρωτόγονος και που “κανένας δεν προσκυνάει” είναι οι μεγάλες κολώνες της ΔΕΗ που βρίσκονται, συνήθως, δίπλα στις εθνικές οδούς.
2. Τα “διάφορα χρώματα που ακουμπούν πάνω σε κάτι γκρίζο που αυτό πατάει πάνω σε γκρίζο χώμα” είναι τα αυτοκίνητα που κινούνται στους αυτοκινητοδρόμους με μεγάλες ταχύτητες, χρησιμοποιώντας τις ρόδες τους, που πατούν πάνω στην άσφαλτο.
3. Στο πρώτο μπαμ ο πρωτόγονος έχει σταματήσει με το ρόπαλό του ένα αυτοκίνητο δημιουργώντας μια μεγάλη καραμπόλα, ενώ ο οδηγός του αυτοκινήτου που χτύπησε, βγήκε από το δρόμο και τράκαρε πάνω σε μια από τις κολώνες της ΔΕΗ και λιποθύμησε. Ο πρωτόγονος συνέρχεται από το χτύπημα λίγο πριν έρθει ένα περιπολικό και ένα ασθενοφόρο με τις σειρήνες τους αναμμένες.
4. Τα “Τοτέμ από ίσια πέτρα” είναι κτήρια και συγκεκριμένα παραλιακές πολυκατοικίες που φυσικά είναι φτιαγμένες από τούβλα, αλλά ο πρωτόγονος δεν μπορεί να το ξέρει αυτό.
5. Λίγο πριν το δεύτερο BANG ο πρωτόγονος χρησιμοποιεί πάλι το ρόπαλό του. Αυτή τη φορά το πετάει, δημιουργώντας πάλι καραμπόλα. Όσο για το κρανίο που βρήκε μετά, ήταν του οδηγού του αυτοκινήτου που χτύπησε.
6. Τέλος, το Γυριστό Αρνί είναι το όνομα ενός, φίλου του, Ινδιάνου. Λόγω Πάσχα... Καταλαβαίνετε...